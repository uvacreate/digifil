import csv, codecs, datetime, psycopg2, spacy, pickle,re, sys,os
from itertools import combinations
from fuzzywuzzy import fuzz
import numpy as np
from datetime import timedelta, date
from collections import Counter


class Movieparser:
    year=0
    city=None
    alpha_regex = re.compile('[^a-zA-Z ]')
    seps = ['muziek', 'concerten', 'tentoonstellingen', 'toneel']
    ignore = ['❖', '*', 'de', 'the', 't', 'en', '»', 'du', 'la', 'd', '°', '«', '"', 'van', '0', 'met', ';', '!', 'o', 'k', 'h', 'b', 'f', ' t', 'm', ':', 's', '20u']
    plaatsen = pickle.load(open('plaatsen.p','rb'))
    variants = {}
    nlp = spacy.load('../selectie/model')
    cinemas_this_city=[]
    cinemas_other_cities=[]
    markers=[]
    unique_to_city=[]
    city_markers=[]
    cur=None
    conn=None
    common_cinemas=[]
    day_order = None
    found_dates = []
    rexcache = {}
    possible_titles=[]
    okdays=[]
    oktitles=[]
    okppn=[]
    datemap={}
    tits={}
    streets=[]
    def __init__(self, city='utrecht', year=1948):
        self.city = city
        self.year = year
        self.setrepl()
        self.plaatsen = [p.lower() for p in self.plaatsen]
        self.plaatsen.remove('hoeven')
        self.plaatsen.remove('monster')
        self.plaatsen.append('zaandam')
        self.plaatsen.append('katwijk')
        self.plaatsen.append('volendam')
        self.plaatsen.append('middelburg')
        self.plaatsen.append('hengelo')
        with codecs.open('extra-data/streets.csv','r', encoding='utf-8') as f:
            r=csv.reader(f, delimiter=';')
            for row in r:
                self.streets.append(row[0])
        self.streets = set(self.streets)
        self.conn=psycopg2.connect(database="delpher", user="postgres", password="root",port=5432)
        self.conn.set_client_encoding('UTF8')
        self.cur = self.conn.cursor()
        self.conn2=psycopg2.connect(database="film", user="postgres", password="root",port=5432)
        self.conn2.set_client_encoding('UTF8')
        self.cur2 = self.conn2.cursor()
        self.cins_per_city()
        try:
            self.possible_titles = pickle.load(open('possible_titles/possible_titles_%s_%s.p' % (self.city, self.year),'rb'))
            self.day_order = pickle.load(open('day_order/day_order_%s_%s.p' % (self.city, self.year),'rb'))
        except Exception:
            self.find_available_days()
        weekct=1
        for single_date in self.daterange(date(year, 1,1), date(year+1, 1,1)):
            wd=self.get_weekday(single_date)
            if wd == 'Sun':
                continue
            if wd == 'Fri':
                weekct+=1
            try:
                self.datemap[weekct]
            except KeyError:
                self.datemap[weekct] = {}
            self.datemap[weekct][single_date]= {'wd': self.get_weekday(single_date), 'fnd':[]}
    def makerest(self, rawstring, fnd, fnd2):
        rest = rawstring.lower()#.replace(fnd.lower(),'').replace(fnd2.lower(),'') + '???' + tomatch
        rest = rest.replace(fnd.lower(),'').strip()
        rest = rest.replace(fnd2.lower(),'').strip()
        rest = ' '.join(rest.split(' '))
        for zq in fnd.lower().split(' ') + fnd2.lower().split():
            rawr=r'\b%s\b' % zq
            try:
                rest = re.sub(rawr, '', rest).strip()
            except Exception:
                pass
            rest = ' '.join(rest.split(' '))
            rest=self.docl(rest)
            
        rest=rest.strip()
        tenpct = int(len(rawstring)/10)+1
        
        if rest.strip() and len(rawstring) + tenpct > len(fnd) and len(rawstring) -1*tenpct < len(fnd) and fuzz.ratio(rawstring, fnd) > 80:
            try:
                rest = self.variants[rest]
            except KeyError:
                rest=''
        if rest in ['voiw','volw','aanvtden','aanvangstijden','aanvtijden']:
            rest = ''
        rest = self.docl(rest)
        return rest            
    def normbios(self, bios):
        try:
            bios = self.variants[bios.lower()]
        except KeyError:
            try:
                bios = self.variants[bios.replace('_',' ').strip().lower()]
            except KeyError:
                try:
                    bios = self.variants[bios.lower().replace('_',' ').strip().replace(' ','_')]
                except KeyError:
                    pass
        return bios

    def docl(self, s):
        badsigns=" ~:;'\- ;♦^\"!?,.»*'«■•_.<>[]{}() "
        s = s.replace('leeftden','').strip()
        if 'tentoon' in s:
            s=s[:s.index('tentoon')]
        
        if not s:
            return ''
        if s.startswith('_'):
            s=s[1:]
        s=s.strip()
        s=s.replace('_',' ')
        s= " ".join(s.split(' '))
        s=s.strip()
        for m in ",\" „.;:()[]{}*|":
            if s.startswith(m):
                s=s[1:].strip()
        for m in ",\" „.;:()[]{}*|":
            if s.endswith(m):
                m=m[:-1].strip()
        for q in ['de','het','the','le','la','l','een']:
            if s.endswith(' %s' % q):
                s= "%s %s" % (q, s[:-len(q)].strip())
        if s.endswith(','):
            s=s[:-1].strip()
        while re.search('[0-9]$', s):
            s=s[:-1].strip()
        for xd in ['14','18']:
            if s.startswith(xd):
                s[2:].strip()
        for mm in ['bioscopen._', 'leeft._']:
            if mm in s:
                s=s.replace(mm,'').strip()
        for xd in badsigns:
            while s.startswith(xd):
                s= s[1:].strip()
        for xd in badsigns:
            while s.startswith(xd):
                s= s[1:].strip()
        for xd in badsigns:
            while s.endswith(xd):
                s= s[:-1].strip()
        for xd in badsigns:
            while s.endswith(xd):
                s= s[:-1].strip()
        s=s.replace(' , ',', ')
        s=s.replace(' . ','. ')
        s=s.replace(' - ','')
        s=s.replace('-','')
        if s.endswith(' ,'):
            s=s[:-2].strip()
        if s.endswith(' .'):
            s=s[:-2].strip()
        for xd in badsigns:
            while s.endswith(xd):
                s= s[:-1].strip()
        if s.endswith(':'):
            s=s[:-1].strip()
        return s.strip()

    def find_available_days(self):
        start_date = date(self.year, 1, 1)
        end_date = date(self.year+1, 1, 1)
        found_days = []
        ct=0
        possible_titles=[]
        for single_date in self.daterange(start_date, end_date):
            if self.get_weekday(single_date) == 'Sun':
                continue
            # print(single_date)
            self.cur.execute("select n.text, a.datum, ppn, a.title from alleladders a left join newarticles n on a.aid = n.id where a.datum = '%s'" % single_date)
            row = self.cur.fetchone()
            
            while row:
                if row:
                    # metabiossen=self.proc2(row[0])
                    metabiossen=self.proc(row[0])
                    for qq in metabiossen:
                        biossen, city=qq
                        curpos=0
                        prevv=''
                        found=[]
                        for pos, b in sorted(biossen.items()):
                            b=self.normname(b)
                            try:
                                b = self.variants[b.lower()]
                            except KeyError:
                                pass
                            found.append(b)
                    score = 0
                    okfound=[]
                    for x in found:
                        if x in self.unique_to_city:
                            score +=1
                            okfound.append(x)
                            found_days.append(self.get_weekday(single_date))
                            possible_titles.append([self.get_weekday(single_date), single_date, x, self.remdatefromtitle(row[-1]), row[2]])
                row = self.cur.fetchone()
        self.day_order = Counter(found_days)
        self.possible_titles = possible_titles
        pickle.dump(self.day_order, open('day_order/day_order_%s_%s.p' % (self.city, self.year),'wb'))
        pickle.dump(self.possible_titles, open('possible_titles/possible_titles_%s_%s.p' % (self.city, self.year),'wb'))
        
    def get_weekday(self, single_date):
        ca = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        return ca[single_date.weekday()]
        
    def daterange(self, start_date, end_date):
        for n in range(int ((end_date - start_date).days)):
            yield start_date + timedelta(n)
    def normname(self, s):
        s=s.strip()
        s=s.replace('„',' ')
        if '(' in s:
            s=s.split('(')[0].strip()
        s = " ".join(s.split())
        if s.endswith(' iv'):
            s=s[:-3]
        if s.endswith(' ii'):
            s=s[:-3]
        if re.search(' [iv0-9]$',s):
            s=s[:-2]
        if s.startswith('t '):
            s=s[2:]
        if s.startswith('de '):
            s=s[3:]
        if s.startswith('du '):
            s=s[3:]
        if s.startswith('the '):
            s=s[4:]
        if s.startswith('dn '):
            s=s[3:]
        s=s.replace(' theater','')
        s=s.replace(' - ','-')
        s=s.replace('cinema ','')
        for ss in self.streets:
            s=s.replace(ss, ' ')
        s = " ".join(s.split())
        s=s.strip()
        try:
            s=self.variants[s.lower()]
        except KeyError:
            pass
        return s.strip()

    def setmarkers(self):
        if self.city == 'utrecht':
            self.markers += ['domkerk', 'tivoli','utrecht']
            try:
                self.unique_to_city.remove('scala')#Theater in Den Haag heet ook zo
            except Exception:
                pass
    def cins_per_city(self):
        
        with codecs.open('extra-data/allcinemas_w_dates.csv','r', encoding='utf-8') as f:
            r=csv.reader(f, delimiter=';')
            for row in r:
                isok=False
                # print(row)
                open = datetime.datetime.strptime(row[2].strip(), '%Y-%m-%d')
                if open <= datetime.datetime.strptime('%s-12-31' % self.year, '%Y-%m-%d'):
                    close = datetime.datetime.strptime(row[3].strip(), '%Y-%m-%d')
                    if close >= datetime.datetime.strptime('%s-01-01' % self.year, '%Y-%m-%d'):
                        isok=True
                if not isok and row[4].strip():
                    open = datetime.datetime.strptime(row[4].strip(), '%Y-%m-%d')
                    if open <= datetime.datetime.strptime('%s-12-31' % self.year, '%Y-%m-%d'):
                        close = datetime.datetime.strptime(row[5].strip(), '%Y-%m-%d')
                        if close >= datetime.datetime.strptime('%s-01-01' % self.year, '%Y-%m-%d'):
                            isok=True
                
                if isok:
                    if row[0]==self.city:
                        self.cinemas_this_city.append(row[1])
                    else:
                        self.cinemas_other_cities.append(row[1])
        self.common_cinemas = set(self.cinemas_this_city).intersection(set(self.cinemas_other_cities))
        self.unique_to_city = set(self.cinemas_this_city) - self.common_cinemas
        self.city_markers=list(self.unique_to_city)
        self.setmarkers()
        self.city_markers += self.markers
        self.city_markers=list(set(self.city_markers))
    def setrepl(self):
        du = [",",":",";","*","~","-",'|','*','•','\\','^','-',' ']
        with codecs.open('extra-data/cinema_name_variations.csv','rb', encoding='utf-8') as f:
            r=csv.reader(f, delimiter=';')
            for row in r:
                if row[4] =='':
                    if not row[3] =='':
                        if not row[1] =='':#DO NOT CLEAN the cinema names
                            # cl3=self.docl(row[3])
                            row[1] = row[1].replace('|','\|')
                            self.variants[row[1].lower()]=row[3].lower()
                            self.variants[row[3].lower()]=row[3].lower()
                            for baa in du:
                                self.variants["%s%s" % (baa, row[3].lower())]=row[3].lower()
                                self.variants["%s%s" % (row[3].lower(),baa)]=row[3].lower()
                            for baa in combinations(du,2):
                                self.variants["%s%s%s" % (baa[0], baa[1], row[3].lower())]=row[3].lower()
                                self.variants["%s%s%s" % (row[3].lower(), baa[0], baa[1])]=row[3].lower()
                            self.variants["%s en" % (row[3].lower())]=row[3].lower()
                            self.variants[row[1].lower().replace(' ','_')]=row[3].lower()
                            self.variants[row[3].lower().replace(' ','_')]=row[3].lower()
                elif row[4] == 'x':
                    if not row[3] =='':
                        if not row[1] =='':
                            self.seps.append("%s 1" % row[3])
                            self.seps.append("%s 2" % row[3])
                            self.seps.append("%s 3" % row[3])
                            self.seps.append("%s 4" % row[3])
                            self.seps.append("%s 5" % row[3])
                            self.seps.append("%s1" % row[3])
                            self.seps.append("%s2" % row[3])
                            self.seps.append("%s3" % row[3])
                            self.seps.append("%s4" % row[3])
                            self.seps.append("%si" % row[3])
                            self.seps.append(row[3])
                            self.seps.append(row[4])
                            self.seps.append("%s 1" % row[4])
                            self.seps.append("%s 2" % row[4])
                            self.seps.append("%s 3" % row[4])
                            self.seps.append("%s 4" % row[4])
                            self.seps.append("%s 5" % row[4])
                            self.seps.append("%s1" % row[4])
                            self.seps.append("%s2" % row[4])
                            self.seps.append("%s3" % row[4])
                            self.seps.append("%s4" % row[4])
                            self.seps.append("%si" % row[4])
        self.seps=list(set(self.seps))


    def cl(self, s):
        s=str(s)
        s=s.strip()
        s=self.alpha_regex.sub('', s)
        s=" ".join(s.split())
        return s.strip()
    def postcorrOLD(self, doc):
        ndoc={}
        
        for j, t in enumerate(doc):
            tag = t.tag_
            txt =self.cl(t.text)
            if not tag == 'B' and txt in allcins:
                tag = 'B'
            if txt in seps:
                tag = 'E'
            elif txt in plaatsen:
                tag = 'E'
            if not tag == t.tag_:
                ndoc[j] = tag
        return ndoc
    def postcorr(self, doc):
        ndoc=[]
        biosjes = set(list(self.variants.values()))
        potential_missers = []
        for j, t in enumerate(doc):
            tag = t.tag_
            txt=t.text
            if txt.lower() == self.city:
                ndoc=[]
            if txt.lower() in self.seps:
                tag = 'E'
            elif txt.lower() in self.plaatsen:
                tag = 'CITY'
            elif tag == 'B':
                try:
                    self.variants[txt.lower()]
                except KeyError:
                    tag = 'T'#Make it a title, so it shows up
                    potential_missers.append([txt])
            ndoc.append([t.text, tag, j])
        return ndoc
    def makerexsafe(self, s):
        return re.escape(s)
    def repl(self, m):
        zz=m.group(0)
        return self.replval
    def remove_crud(self, txt):
        txt=txt.replace(':.', ': ')
        for k in ['geprolongeerd']:
            rex = re.compile(r'\b%s\b' % self.makerexsafe(k))
            txt = re.sub(rex, ' ', txt)
        return " ".join(txt.split(' '))
    def fix_cinema_names(self, txt):
        #replace all the mangled cinema names with corrected ones
        txt = txt.replace('_',' ')
        toadd=[]
        xs=list(self.variants.keys())
        xs.sort(key = lambda s: len(s), reverse=True)
        for k in xs:
            v = self.variants[k]
            k=k.replace('_',' ')
            v=v.replace('_',' ')
            try:
                rex = self.rexcache[k]
            except KeyError:
                rex = re.compile(r'\b%s\b' % self.makerexsafe(k))
                self.rexcache[k]=v.replace(' ','_')
                try:
                    self.variants[v.replace(' ','_')]
                except KeyError:
                    toadd.append(v.replace(' ','_'))
            self.replval = v.replace(' ','_')
            txt = re.sub(rex, self.repl, txt)
        for moo in toadd:
            self.variants[moo] = moo
        txt=txt.replace(' * ',' ')
        txt=txt.replace(' .','.')
        txt=txt.replace(' ,',',')
        txt=txt.replace(' :',':')
        txt=txt.replace(' v d ',' van de ')
        txt=txt.replace(' \' t ',' het ')
        txt=txt.replace(' ;',';')
        txt = " ".join(txt.split(' ')).strip()
        return txt
    def proctest(self, txt, debug=False):
        txt = self.remove_crud(txt)
        txt = self.fix_cinema_names(txt)
        doc = self.nlp(txt)
        doc = self.postcorr(doc)
        
        curtag = None
        curtit = []
        curbios = None
        curcity = None
        days = []
        xtra = []
        wholething=[]
        prevtext = None
        for j ,x in enumerate(doc):
            if curtag == 'B' and x[0] == 'en':
                x[1] = 'B'
            if debug or x[0] in ['colosseum','en','victoria']:
                print('>>',x)
            if x[1] == 'B':
                
                if curtag == 'B':
                    curbios = "%s %s" % (curbios, x[0])
                    print('-->', curbios)
                else:
                    wholething.append([curbios, " ".join(curtit), curcity, days, xtra])
                    # print([curbios, " ".join(curtit), curcity, days, xtra])
                    curbios = x[0]
                print(curbios)
                curtit = []
                days=[]
                xtra = []
            elif x[1] in ['ST','E', 'CITY']:
                if x[1] == 'CITY':
                    curcity=x[0]
                curbios=None
                curtit = []
                days=[]
                xtra = []
            elif x[1] == 'T':
                doingtit=True
                curtit.append(x[0])
            elif x[1] in ['D','H']:
                if not x[0] in ['u',',',':','u.','uur']:
                    days.append(x[0])
            else:
                xtra.append(x[0])
            curtag = x[1]
            prevtext = x[0]
    def proc2(self, txt, debug=False):
        txt = re.sub('den haag',' sgravenhage ', txt, re.IGNORECASE)
        txt = re.sub(' rex',' rex ', txt, re.IGNORECASE)
        txt = re.sub('luxor',' luxor ', txt, re.IGNORECASE)
        txt = self.remove_crud(txt)
        txt = self.fix_cinema_names(txt)
        txt = txt.replace('.','. ')
        doc = self.nlp(txt)
        ddoc=[]
        for j, t in enumerate(doc):
            ddoc.append([t.text, t.tag_, j])
        if False:
            doc2 = self.nlp(txt)
            ddoc=[]
            doc = self.postcorr(doc)
            
            ddoc2=[]
            for j, t in enumerate(doc2):
                ddoc2.append([t.text, t.tag_, j])
            for kj, z in enumerate(doc):
                try:
                    dtest=z[0].upper().replace('.','').strip()
                    d2test=ddoc2[kj][0].upper().replace('.','').strip()
                    if not dtest == d2test:
                        if len(ddoc2) > kj+1 and z[0].upper() == ddoc2[kj+1][0].upper():
                            ddoc2 = ddoc2[:kj-1] + ddoc2[kj:]
                        elif z[0].upper() == ddoc2[kj-1][0].upper():
                            ddoc2 = ddoc2[:kj] + [' ']+ ddoc2[kj:]
                except IndexError:
                    pass
            for kj, z in enumerate(doc):
                try:
                    dtest=z[0].upper().replace('.','').strip()
                    d2test=ddoc2[kj][0].upper().replace('.','').strip()
                    if not dtest == d2test:
                        if len(ddoc2) > kj+1 and z[0].upper() == ddoc2[kj+1][0].upper():
                            ddoc2 = ddoc2[:kj-1] + ddoc2[kj:]
                        elif z[0].upper() == ddoc2[kj-1][0].upper():
                            ddoc2 = ddoc2[:kj] + [' ']+ ddoc2[kj:]
                except IndexError:
                    pass
            for kj, z in enumerate(doc):
                try:
                    ddoc.append([z[0], z[1], ddoc2[kj], z[2]])
                    dtest=z[0].upper().replace('.','').strip()
                    d2test=ddoc2[kj][0].upper().replace('.','').strip()
                    if not dtest == d2test:
                        print('>>>',ddoc[kj-3],ddoc2[kj-3])
                        print('>>>',ddoc[kj-2],ddoc2[kj-2])
                        print('>>>',ddoc[kj-1],ddoc2[kj-1])
                        print('>>>',z,ddoc2[kj])
                        try:
                            print('>>>',ddoc[kj+1],ddoc2[kj+1])
                            print('>>>',ddoc[kj+2],ddoc2[kj+2])
                            print('??',dtest,d2test)
                            sys.exit('UPPERMISMATCH')
                        except IndexError:#End of thing, don't care
                            pass
                except IndexError:#End of thing, don't care
                    pass
        curtag = None
        curtit = []
        curbios = None
        curcity = None
        days = []
        xtra = []
        wholething=[]
        kk=list(set(self.variants.keys()))
        startpos=None
        for jq, x in enumerate(ddoc):
            if not startpos:
                startpos = x[2]
            if x[0].lower() in kk:
                if self.variants[x[0].lower()] == 'xx':
                    x[1] = 'E'
                else:
                    x[1] = 'B'
            elif curtag == 'B' and x[0] == 'en':
                x[1] = 'B'
            elif x[0] == 'B':
                x[0] = 'T'
            if debug:
                print('>>',x)
            if x[1] == 'B':
                
                if curtag == 'B':
                    curbios = "%s %s" % (curbios, x[0])
                else:
                    bioskes = [curbios]
                    if curbios and ' en 'in curbios:
                        bioskes = curbios.split(' en ')
                    for bb in bioskes:
                        wholething.append([bb, " ".join(curtit), curcity, days, xtra, startpos])
                        startpos = x[2]+1
                    curbios = x[0]
                curtit = []
                days=[]
                xtra = []
            elif x[1] in ['ST','E', 'CITY']:
                if x[1] == 'CITY':
                    curcity=x[0].lower()
                curbios=None
                curtit = []
                days=[]
                xtra = []
            elif x[1] == 'T':
                doingtit=True
                curtit.append(x[0])
            elif x[1] in ['D','H']:
                if not x[0] in ['u',',',':','u.','uur']:
                    days.append(x[0])
            else:
                xtra.append(x[0])
            curtag = x[1]
        
        bioskes = [curbios]
        if curbios and ' en 'in curbios:
            bioskes = curbios.split(' en ')
        for bb in bioskes:
            wholething.append([bb, " ".join(curtit), curcity, days, xtra, startpos])
        if debug:
            sys.exit()
        return wholething      
    def isbadtit(self, tit):
        if not tit.strip():
            return True
        for m in ['aquarellen','fotogrammen','orkest','schilderijen','houtsneden', 'werken', 'schilderyen', 'foto s','tentoonstelling','expositie','toneel', 'theater','muziek', 'revue', 'klucht']:
            if m in tit:
                return True
        return False
    def proc(self, txt):
        doc = self.nlp(txt)
        corrdocr = self.postcorr(doc)
        corrdoc={}
        for cc in corrdocr:
            corrdoc[cc[0]] = cc[1]
        curbios = None
        curtit=[]
        findingbios=False
        doingtit=False
        biossen=[]
        biospos=[]
        metabiossen = []
        city=None
        prevcity=None
        for j, t in enumerate(doc):
            try:
                t.tag_ = corrdoc[j]
            except KeyError:
                pass
            if t.text.lower() in self.seps:
                t.tag_ = 'E'
            if self.cl(t.text.lower()) in self.plaatsen:
                t.tag_ = 'E'
                prevcity=city
                city = self.cl(t.text.lower())
            if t.tag_ == 'B':
                if t.text.lower() in self.ignore:
                    continue
                if findingbios:
                    curbios = "%s %s" % (curbios, t.text.lower())
                else:
                    curbios = t.text.lower()
                    try:
                        curbios = self.variants[curbios]
                    except KeyError:
                        pass
                findingbios=True
            else:
                findingbios=False
            if t.tag_ in ['ST','E']:
                if not biossen == []:
                    metabiossen.append((dict(zip(biospos,biossen)), prevcity))
                    prevcity=city
                biossen=[]
                biospos=[]
            if t.tag_ == 'T':
                doingtit=True
                curtit.append(t.text.strip())
            elif doingtit and t.tag in ['X','P']:#Rejoin titles separated by just punctuation
                pass
            else:
                doingtit=False
                curtit = [cc for cc in curtit if cc]
                if not curtit == []:
                    lebios = self.cl(curbios)
                    try:
                        if not biossen[-1] == lebios:
                            biossen.append(lebios)
                            biospos.append(j)
                    except IndexError:
                        biossen.append(lebios)
                        biospos.append(j)
                curtit=[]
        if not biossen == []:
            metabiossen.append((dict(zip(biospos,biossen)),prevcity))
        return metabiossen
    def remdatefromtitle(self, tit):
        months = ['JANUARI','FEBRUARI','MAART','APRIL','JULI','JUNI','AUGUSTUS','SEPTEMBER','OKTOBER','NOVEMBER','DECEMBER']
        months += ['FEBR']
        for m in months:
            tit = re.sub(' VAN [0-9]+ TOT [0-9]+ %s( [0-9]+)?' % m, '', tit)
        for m in months:
            tit = re.sub('[0-9]+ %s' % m, '', tit)
        return tit
        
    def find_missing_dates(self, found_dates):
        weekcheck = {}
        missingweeks = {}
        ct=0
        for single_date in n.daterange(date(year, 1,1), date(year+1, 1,1)):
            wd=n.get_weekday(single_date)
            if self.year < 1954:
                if wd == 'Fri':
                    ct+=1
            else:
                if wd == 'Fri':
                    ct+=1
            if wd in okdays:
                if single_date in found_dates:
                    try:
                        weekcheck[wd].append(ct)
                    except KeyError:
                        weekcheck[wd] = [ct]
                else:
                    try:
                        missingweeks[wd].append(ct)
                    except KeyError:
                        missingweeks[wd] = [ct]
        return [weekcheck, missingweeks]
        
    def getmarkers(self):
        vars = []
        varm = {}
        for k,v in self.variants.items():
            # if v in n.unique_to_city:
            if v in self.cinemas_this_city:
                vars.append(k)
            if v in self.city_markers:
                try:
                    varm[v].append(k)
                except KeyError:
                    varm[v] = [k]
        return [vars, varm]
    def setfounddates(self):
        for v in self.possible_titles:
            self.found_dates.append(v[1])
        return self.found_dates

    def setokdays(self):
        q = np.mean(list(self.day_order.values()))
        qq = np.std(list(self.day_order.values()))
        for d, xx in self.day_order.items():
            if xx > q-qq:
                self.okdays.append(d)
        return self.okdays
    def setoktitles(self):
        for v in n.possible_titles:
            if v[0] in okdays:
                self.oktitles.append(v[-2])
                self.okppn.append(v[-1])
        
        self.oktitles=list(set(self.oktitles))
        okppn=list(set(self.okppn))
        return [self.oktitles, self.okppn]
        
    def getmissingladders(self):
        #Find missing ladders
        #First just try finding articles with the city title in the name, filtered fuzzily by the oktitles
        #looking only at PPNs that had success already
        ct=0
        weekcheck = {}
        missingweeks = {}
        for weekno, days in self.datemap.items():
            for day, data in days.items():
                #Only known good days
                if data['wd'] in self.okdays and data['fnd'] == []:
                    self.cur.execute("select n.text, a.datum, a.title, id from alleladders a left join newarticles n on a.aid = n.id where a.datum = '%s' and ppn in ('%s')" % (day, "','".join(self.okppn)))
                    row = self.cur.fetchone()
                    while row:
                        #Check for city name in text
                        ok=False
                        
                        if self.city in row[0].lower():
                            ok=True
                        if self.city =='sgravenhage' and 'den haag' in row[0].lower():
                            ok=True
                        if self.city =='sgravenhage' and 'gravenhage' in row[0].lower():
                            ok=True
                        if ok:
                            top=0
                            #Filter by known good titles
                            for x in self.oktitles:
                                rat = fuzz.ratio(x, row[2])
                                if rat > top:
                                    top=rat
                            if top > 70:
                                metabiossen=self.proc(row[0])
                                for qq in metabiossen:
                                    biossen, lcity=qq
                                    curpos=0
                                    prevv=''
                                    found=[]
                                    for pos, b in sorted(biossen.items()):
                                        b=n.normname(b)
                                        try:
                                            b = n.variants[b]
                                        except KeyError:
                                            pass
                                        found.append(b)
                                        score = 0
                                        for x in found:
                                            if x in n.unique_to_city:
                                                score +=1
                                                self.oktitles.append(row[2])
                                                self.datemap[weekno][day]['fnd'].append(row[-1])
                        row = self.cur.fetchone()

    def getmissingladders_pass_2(self):
        keepgoing=True
        #Fuzzy article title search, ignoring ppns, checking text for mangled cinema names
        vars, varm = self.getmarkers()
        missing = self.getmissingweeks()
        misscount=1
        times=[]
        t1 = datetime.datetime.now()
        for weekno, days in self.datemap.items():
            if not weekno in missing:
                continue
                
            t2 = datetime.datetime.now()
            delta = t2 - t1
            t1=t2
            times.append(delta.seconds)
            remaining = (len(missing)/1)*(np.mean(times)/60)
            print("Pass 2: week %s," % weekno, "%s/%s" % (misscount, len(missing)), "%.1f" % (delta.seconds/60), 'min, ETA', "%.1f" % (remaining), "(%.1f hrs)" % (remaining/60))
            misscount+=1
            for day, data in days.items():
                print('--', day)
                self.cur.execute("select text, datum, title, id from newarticles where datum = '%s' " % day)
                row = self.cur.fetchone()
                while row:
                    ok=False
                    for x in set(self.oktitles):
                        if ok:
                            break
                        if not x.strip() or x =='FILM':
                            continue
                        if fuzz.ratio(row[2][:50],x[:50]) > 80:
                            ok=True
                    if ok:
                            
                        score = 0
                        for rrr, xt in varm.items():
                            if score > 1:
                                break
                            for xx in xt:
                                if score > 1:
                                    break
                                if xx.lower() in row[0].replace(' ','').lower() or xx.lower() in row[0].lower():
                                    score+=1
                                    break#Don't find multiple aliases for same cinema
                        if score > 1:
                            self.datemap[weekno][day]['fnd'].append(row[-1])
                            if not row[2] in self.oktitles:
                                self.oktitles.append(row[2])
                    row = self.cur.fetchone()

    def getmissingweeks(self):
        bads=[]
        for k,v in self.datemap.items():
            fnd=False
            for kk, vv in v.items():
                if len(vv['fnd']) > 0:
                    fnd=True
            if not fnd:
                bads.append(k)
        return bads
    
    def normtit(self, t):
        t=t.strip()
        for pp in ['de','het','een', 'le', 'la', 'the','De','Het','Een','Le', 'La', 'The', 'Il', "L'"]:
            if t.endswith(' %s' %pp):
                t='%s %s' % (pp, t[:-1*len(pp)].strip())
        if t.endswith(','):
            t=t[:-1].strip()
            
        return t.strip()
        
    def load_tits(self):
        self.tits = {}#In case it wasn't properly re-initialized
        try:
            tits = pickle.load(open('titcache/tits_latin_%s.p'%self.year,'rb'))
        except Exception:
            tits = pickle.load(open('titcache/tits_%s.p'%self.year,'rb'))
        
            except Exception:
                print("Loading titles....")
                tits = {}
                
                #Add CC no-year titles
                with open('CinemaContext/tblFilmTitleVariation_noyears.csv','r') as f:
                    r=csv.reader(f)
                    for row in r:
                        tit=row[2].lower().replace(' (xx)','')
                        id=row[1]
                        tits[self.normtit(tit)] = id

                for i in range (self.year, self.year-30, -1):
                        self.cur2.execute("select t.title, refid from public.references r left join public.titles t on r.title = t.id where year =%s and src in ('CC','imdb','imdba')" % (i))
                        row = self.cur2.fetchone()
                        while row:
                            tits[self.normtit(row[0])] = row[1]
                            row = self.cur2.fetchone()

                
                if self.year >= 1959:#Add 60s titles
                    print('Add 60s titles')
                    tits60 = pickle.load(open('extra-data/60s_titles.p','rb'))
                    for k,v in tits60.items():
                        if v[1]<= self.year:
                            tits[self.normtit(k)] = v[0]
                if self.year >= 1970:#Add 60s titles
                    print('Add 70s titles')
                    tits60 = pickle.load(open('extra-data/70s_titles.p','rb'))
                    for k,v in tits60.items():
                        if v[1]<= self.year:
                            tits[self.normtit(k)] = v[0]

                #Remove some interfering titles
                tits.pop('de',None)
                tits.pop('en',None)
                tits.pop('film',None)
                tits.pop('',None)

                            
                #Add title variations from CC, ignoring years again
                with codecs.open('CinemaContext/tblFilmTitleVariation.csv','r', encoding='utf-8') as f:
                    r=csv.reader(f, delimiter=',')
                    for row in r:
                        tits[row[2].lower().strip()] = row[1]
                #Add title variations
                #Add manually found titles, not bothering with years
                with codecs.open('extra-data/extra_titels.csv','r', encoding='utf-8') as f:
                    r=csv.reader(f, delimiter=';')
                    for row in r:
                        tits[row[0].strip()] = int(row[1].split('tt')[2].split('/')[0])
                #Set some 'titles' manually
                tits['tekenfilm'] = 'tekenfilm'
                tits['journaal'] = 'journaal'
                tits['actualiteiten'] = 'actualiteiten'
                tits['nieuws en actualiteiten'] = 'nieuws en actualiteiten'
                tits['nederlands en wereldnieuws, tekenfilm'] = 'nederlands en wereldnieuws, tekenfilm'
                pickle.dump(tits, open('titcache/tits_%s.p'%self.year,'wb'))
        print("Titles loaded!", self.year)
        self.tits = tits

if __name__ == '__main__':

    pass