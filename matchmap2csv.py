import csv, pickle,codecs, os,sys, psycopg2

conn2=psycopg2.connect(database="film", user="postgres", password="root",port=5432)
conn2.set_client_encoding('UTF8')
cur = conn2.cursor()
xmap={}
for b in os.listdir('matchmap/check/'):
    with codecs.open('matchmap/check/%s' % b,'r', encoding='utf-8') as f:
        r=csv.DictReader(f,delimiter=';')
        for row in r:
            if row['isok'] == 'x':
                try:
                    xmap[row['part_title']] += 1
                except KeyError:
                    xmap[row['part_title']] = 1

year=1977
ct={}
for city in ['sgravenhage','amsterdam','rotterdam','utrecht']:
    with codecs.open('matches/%s/filtered/separated_titles/replaced_titles/matched/%s_%s.csv' % (city,city,year),'r', encoding='utf-8') as f:
        r=csv.DictReader(f,delimiter=';')
        for row in r:
            try:
                ct[row['title2'].lower()]+=1
            except KeyError:
                ct[row['title2'].lower()]=1
        
ccmap={}
with codecs.open('tblFilm.csv','r', encoding='utf-8') as f:
    r=csv.DictReader(f)
    for row in r:
        if row['imdb'].strip():
            ccmap[row['film_id']] = int(row['imdb'].replace('/','').replace('s',''))

out=[]
known=[]
try:
    dta = pickle.load(open('matchmap/%s.p' % (year-1),'rb'))
    known=[]
    for k,v in dta.items():
        imdb=None
        try:
            imdb = int(v[2])
        except ValueError:
            try:
                imdb = ccmap[v[2]]
            except KeyError as err:
                pass
        if imdb:
            known.append(imdb)
except FileNotFoundError:
    pass
dta = pickle.load(open('matchmap/%s.p' % year,'rb'))
kn={}
print("Start")
cct=0
idcount={}
for k,v in dta.items():
    imdb=None
    try:
        imdb = int(v[2])#Imdb ID
    except ValueError:
        try:
            imdb = ccmap[v[2]]
        except KeyError as err:
            pass
    try:
        idcount[imdb]+=1
    except KeyError:
        idcount[imdb]=1
for k,v in dta.items():
    cct+=1
    if cct % 50 == True:
        print(cct, len(dta))
    imdb=None
    try:
        imdb = int(v[2])
    except ValueError:
        try:
            imdb = ccmap[v[2]]
        except KeyError as err:
            # print('err',err)
            pass
    try:
        ya=ct[k]
    except KeyError:
        ya=None
    prevok=None
    if imdb in known:
        prevok=1
    try:
        row['match_year'] = kn[imdb]
    except KeyError:
        cur.execute("select year from public.references where src in ('imdb', 'imdba') and refid = '%s' limit 1" % imdb)
        res = cur.fetchone()
        if not res:
            cur.execute("select year from public.references where src = 'CC' and refid = '%s' limit 1" % imdb)
            res = cur.fetchone()
        if res:
            kn[imdb]=res[0]
        else:
            kn[imdb]=None
    isok=None
    try:
        if xmap[v[1]] > 1:
            isok='x'
            # print("X")
    except KeyError:
        pass
    unique=None
    if idcount[imdb] == 1 and isok is None and ya == 1:
        isok = 0
    yur = {'full_title':k,'ratio':v[0], 'part_title': v[1],'rawid':v[2],'year':kn[imdb],'imdbid':imdb,'count':ya,'exists_in_prev':prevok,'isok':isok}
    out.append(yur)

with codecs.open('matchmap/check/%s.csv' % year,'wb', encoding='utf-8') as f:
    w=csv.DictWriter(f,delimiter=';', fieldnames=['full_title','ratio','part_title','rawid','year','imdbid','count','exists_in_prev','isok'])
    w.writeheader()
    w.writerows(out)