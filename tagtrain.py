#!/usr/bin/env python
# coding: utf8
"""
A simple example for training a part-of-speech tagger with a custom tag map.
To allow us to update the tag map with our custom one, this example starts off
with a blank Language class and modifies its defaults. For more details, see
the documentation:
* Training: https://spacy.io/usage/training
* POS Tagging: https://spacy.io/usage/linguistic-features#pos-tagging

Compatible with: spaCy v2.0.0+
"""
from __future__ import unicode_literals, print_function
import sys, psycopg2, csv, codecs, os
import plac
import random
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding

nlp = spacy.blank('nl')
tokenizer = nlp.Defaults.create_tokenizer(nlp)


known=[]
for x in os.listdir('pos_training_files'):
    if x.startswith('.'):
        continue
    x=x.replace('.NEW','')
    known.append(x[:-4])

# You need to define a mapping from your data's part-of-speech tag names to the
# Universal Part-of-Speech tag set, as spaCy includes an enum of these tags.
# See here for the Universal Tag Set:
# http://universaldependencies.github.io/docs/u/pos/index.html
# You may also specify morphological features for your tags, from the universal
# scheme.
TAG_MAP_RAW = {
    'B': {'pos': 'ADJ','name':'BIOS'},
    'T': {'pos': 'ADP','name':'TITLE'},
    'H': {'pos': 'AUX','name':'HOUR'},
    'C': {'pos': 'DET','name':'CITY'},
    'D': {'pos': 'NUM','name':'DAY'},
    'TEL': {'pos': 'PART','name':'TELEPHONE'},
    'S': {'pos': 'PROPN','name':'STREET'},
    'X': {'pos': 'X','name':'CRUFT'},
    'L': {'pos': 'INTJ','name': 'AGE'},
    'E': {'pos': 'VERB','name':'END_MOVIES'},
    'P': {'pos':'PUNCT', 'name':'SEPARATOR'},
    'SS': {'pos': 'PRON', 'name': 'SUBSECTION'},
    'ST': {'pos': 'SYM','name':'START_MOVIES'}
}

TAG_MAP={}
for k,v in TAG_MAP_RAW.items():
    TAG_MAP[k] = {'pos': v['pos']}


TRAIN_DATA = []
ct=0
taglist={}
dosave=True
known=[]
for x in os.listdir('pos_training_files'):
    if x.startswith('.') or 'NEW' in x:
        continue
    ct+=1
    print(x)
    with codecs.open('pos_training_files/%s' % x, 'r', encoding='utf-8') as f:
        r=csv.reader(f, delimiter=';')
        stri=[]
        tags=[]
        for row in r:
            # if not row[0].strip() or not row[1].strip():
                # continue
            # if row[1] in ['T']:
                # continue
            #if not row[1] in ['TEL', 'L', 'P', 'T', 'X', 'ST', 'B', 'H', 'S']:
            #    continue
            
            
            if row[1] in ['0','',' ']:
                ta = 'X'
            else:
                ta=row[1]
            stri.append(row[0])
            tags.append(ta)
            try:
                taglist[ta]
            except KeyError:
                taglist[ta]={}
            try:
                taglist[ta][row[0]].append(x)
            except KeyError:
                taglist[ta][row[0]] = [x]

        for tag in tags:
            if not tag in TAG_MAP.keys():
                print(x, tag)
        TRAIN_DATA.append((" ".join(stri), {'tags': tags}))
for k,v in taglist.items():
    yout=[]
    for kk, vv in v.items():
        yout.append([kk, len(vv)]+vv)
    with codecs.open('taggather/%s.csv'%k, 'w', encoding='utf-8') as f:
        w=csv.writer(f, delimiter=';')
        w.writerows(yout)

@plac.annotations(
    lang=("ISO Code of language to use", "option", "l", str),
    output_dir=("Optional output directory", "option", "o", Path),
    n_iter=("Number of training iterations", "option", "n", int))
def main(lang='en', output_dir=None, n_iter=25):
    """Create a new model, set up the pipeline and train the tagger. In order to
    train the tagger with a custom tag map, we're creating a new Language
    instance with a custom vocab.
    """
    nlp = spacy.blank(lang)
    # add the tagger to the pipeline
    # nlp.create_pipe works for built-ins that are registered with spaCy
    tagger = nlp.create_pipe('tagger')
    # Add the tags. This needs to be done before you start training.
    for tag, values in TAG_MAP.items():
        tagger.add_label(tag, values)
    nlp.add_pipe(tagger)

    optimizer = nlp.begin_training()
    for i in range(n_iter):
        random.shuffle(TRAIN_DATA)
        losses = {}
        # batch up the examples using spaCy's minibatch
        batches = minibatch(TRAIN_DATA, size=compounding(4., 32., 1.001))
        for batch in batches:
            texts, annotations = zip(*batch)
            nlp.update(texts, annotations, sgd=optimizer, losses=losses)
        print('Losses', losses)

    # save model to output directory
    output_dir='model'
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)



if __name__ == '__main__':
    plac.call(main)
