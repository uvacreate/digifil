from matplotlib import pyplot as plt
import psycopg2, re
from datetime import timedelta, date


cities=[
'ROTTERDAM','AMSTERDAM','UTRECHT','DEN HAAG'
]
fconn=psycopg2.connect(database="film", user="postgres", password="root",port=5432)
fconn.set_client_encoding('UTF8')
fcur = fconn.cursor()

conn=psycopg2.connect(database="delpher", user="postgres", password="root",port=5432)
conn.set_client_encoding('UTF8')
cur = conn.cursor()
conn2=psycopg2.connect(database="delpher", user="postgres", password="root",port=5432)
conn2.set_client_encoding('UTF8')
cur2 = conn2.cursor()
for yr in range(1948, 1996):
    print(yr)
    fcur.execute("select * from public.references r left join titles t on r.title = t.id where year <= %s and year > %s and src in ('imdb','imdba','CC')", (yr, yr-2))
    row = fcur.fetchone()
    want = re.compile('[aeiou]{1,}')
    titset = []
    while(row):
        if ' ' in row[-1] or len(row[-1]) >5:
            test = re.search(want, row[-1])
            if test:
                titset.append(row[-1])
        row = fcur.fetchone()

    for city in cities:

        cur.execute("select text, a.title, l.city, l.datum, l.aid from ladders l left join newarticles a on l.aid = a.id where date_part('year', l.datum) = '%s' and l.city like '%s' and score_titles IS NULL"% (yr,city))
        row = cur.fetchone()
        while(row):
            txt = row[0].lower()
            score=0
            for tit in titset:
                if tit in txt:
                    score+=1
            cur2.execute("update ladders set score_titles = %s where city=%s and datum = %s and aid = %s", (score, row[-3], row[-2], row[-1]))
            conn2.commit()
            row = cur.fetchone()