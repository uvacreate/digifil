import csv, psycopg2

mappy={}
with open('CinemaContext/tblVenueActivePeriode.csv','r') as f:
    r=csv.reader(f)
    for row in r:
        cin = row[7]
        if '(' in row[7]:
            cin = row[7].replace(')','').replace('(',' ')
        cin = " ".join(cin.split(' '))
        cin = [c.lower() for c in cin.split(' ') if c.strip() and not c in ['-']]
        city = row[18].upper()
        if not row[2].strip():
            row[2] = '2011'
        try:
            mappy[city]
        except KeyError:
            mappy[city] = {}
        for i in range(int(row[1][:4]), int(row[2][:4])+1):
            try:
                mappy[city][i] += cin
            except KeyError:
                mappy[city][i] = cin
            mappy[city][i] = list(set(mappy[city][i]))

cities=[
'ROTTERDAM','AMSTERDAM','UTRECHT','DEN HAAG'
]
conn=psycopg2.connect(database="delpher", user="postgres", password="root",port=5432)
conn.set_client_encoding('UTF8')
cur = conn.cursor()

conn2=psycopg2.connect(database="delpher", user="postgres", password="root",port=5432)
conn2.set_client_encoding('UTF8')
cur2 = conn2.cursor()
for yr in range(1948, 1996):

    for city in cities:
        print(yr, city)
        cur.execute("select text, a.title, l.city, l.datum, l.aid from ladders l left join newarticles a on l.aid = a.id where date_part('year', l.datum) = '%s' and l.city like '%s' and score_bios IS NULL"% (yr,city))
        row = cur.fetchone()
        while(row):
            txt = row[0].lower()
            score=0.00
            val=1/len(mappy[city][yr])
            for tit in mappy[city][yr]:
                if tit in txt:
                    score+=val
            score=round(score,2)
            cur2.execute("update ladders set score_bios = %s where city=%s and datum = %s and aid = %s", (score, row[-3], row[-2], row[-1]))
            conn2.commit()
            row = cur.fetchone()