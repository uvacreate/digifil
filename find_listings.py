import pickle, sys, psycopg2, codecs,csv, os
import numpy as np
from parserclass import Movieparser
from datetime import datetime
from multiprocessing import Process, Queue, current_process, freeze_support

def worker(input, output, city, year):
    
    nar = Movieparser(city, year)
    conn=psycopg2.connect(database="delpher", user="postgres", password="root",port=5432)
    conn.set_client_encoding('UTF8')
    cur = conn.cursor()
    badcins=[]
    for row in iter(input.get, 'STOP'):
        ret = []
        weekno = row[0]
        day=row[1]
        data = row[2]
        if not data['fnd'] == []:   
            data['fnd'] = list(set(data['fnd']))
            prev = None
            aids = ",".join([str(a) for a in set(data['fnd'])])
            cur.execute("select title, text, url, id from newarticles where id in (%s)" % aids)
            row = cur.fetchone()
            found_urls=[]
            while row:
                if row[2] in found_urls:
                    continue
                found_urls.append(row[2])
                prev = row[2]
                debug=False
                text = row[1]
                article_id=row[3]
                goo = nar.proc2(text)
                for j, ldata in enumerate(goo):
                    try:
                        try:
                            ldata[0] = nar.variants[ldata[0].lower()]
                        except KeyError:
                            try:
                                badcins = pickle.load(open('badcins.p','rb'))
                                badcins.append(ldata[0])
                                pickle.dump(badcins, open('badcins.p','wb'))
                            except Exception:
                                pass

                    except AttributeError:
                        pass
                        
                    ret.append([weekno, day, article_id, row[2]] + ldata)
                row = cur.fetchone()
        output.put(ret)
        

if __name__ == '__main__':
    NUMBER_OF_PROCESSES = 6
    step=10
    tt1 = datetime.now()
    for year in range(1948, 1996):
        for city in ['sgravenhage','amsterdam','rotterdam','utrecht']:
            
            if os.path.isfile('matches/%s/%s_%s.csv' % (city, city, year)):
                continue
            print(city, year)
            try:
                TASK_DICT = pickle.load(open('%s/%s_%s.p' % (city, city, year), 'rb'))
            except FileNotFoundError:
                print('NOT READY')
                continue
            pickle.dump([], open('badcins.p','wb'))
            tot=0
            TASKS = []
            tm=[]
            for k,v in TASK_DICT.items():
                for day, data in v.items():
                    if day.year == year:
                        tot+=len(set(data['fnd']))
                        tm.append(len(set(data['fnd'])))
                        TASKS.append((k,day,data))
               
            task_queue = Queue()
            done_queue = Queue()
            for task in TASKS:
                task_queue.put(task)
            nar = Movieparser(city, year)
            print("Total to do:", len(TASKS), tot)
            
            for i in range(NUMBER_OF_PROCESSES):
                Process(target=worker, args=(task_queue, done_queue, city, year)).start()
            yact=0
            out=[
                ['weekno','date','article_id', 'url', 'cinema','title','iets','times', 'ages','startposition']
            ]
            times=[]
            t1 = datetime.now()
            toty=0
            for i in range(len(TASKS)):
                yact+=1
                for z in done_queue.get():
                    out.append(z)
                try:
                    toty+=tm[i]
                except Exception:
                    pass
                
                if yact%step==True and yact>1:
                    t2 = datetime.now()
                    delta = t2 - t1
                    t1=t2
                    times.append(delta.seconds)
                    todo = len(TASKS) - yact
                    remaining=1
                    remaining = ((tot-toty)/step)*(np.mean(times)/60)
                    print("%s/%s done in" % (yact, len(TASKS)), "%.1f" % (delta.seconds/60), 'min, ETA', "%.1f" % (remaining),"(%.1f h)" % (remaining/60), '%s/%s'%(toty,tot))
                
            
            for i in range(NUMBER_OF_PROCESSES):
                task_queue.put('STOP')
            with codecs.open('matches/%s/%s_%s.csv' % (city, city, year), 'w', encoding='utf-8') as f:
                w=csv.writer(f, delimiter=';')
                w.writerows(out)
                
            tt2 = datetime.now()
            delta = tt2 - tt1
            print("Total took", "%.1f" % (delta.seconds/60),"min")
            print('done...')
            