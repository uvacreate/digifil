#!/usr/bin/env python
# coding: utf8
"""
A simple example for training a part-of-speech tagger with a custom tag map.
To allow us to update the tag map with our custom one, this example starts off
with a blank Language class and modifies its defaults. For more details, see
the documentation:
* Training: https://spacy.io/usage/training
* POS Tagging: https://spacy.io/usage/linguistic-features#pos-tagging

Compatible with: spaCy v2.0.0+
"""
from __future__ import unicode_literals, print_function
import sys, psycopg2, csv, codecs, os
import plac
import random, xlrd
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding
import numpy as np
import xlwt
from colour import Color


nlp = spacy.blank('nl')
tokenizer = nlp.Defaults.create_tokenizer(nlp)
book = xlwt.Workbook(encoding="UTF-8")



TEST_DATA=[]
known=[]
#To avoid adding an already tagged file to the new training files
for x in os.listdir('pos_training_files'):
    if x.startswith('.'):
        continue
    known.append(int(x[:-4]))

known=list(set(known))    

# #Read all the Excel files 
# for x in os.listdir('xlsout'):
    # if x.endswith('.xls'):
        # bookie = xlrd.open_workbook('xlsout/%s' % x)
    # for sheet in bookie.sheets():
        # known.append(int(sheet.name))

if True:
    conn=psycopg2.connect(database="delpher", user="postgres", password="root",port=5432)
    conn.set_client_encoding('UTF8')
    cur = conn.cursor()

    cur.execute("select text, aid from ladders l left join newarticles a on l.aid = a.id where score_bios > 0.5 and score_titles > 1  order by random() limit 100" )
    row = cur.fetchone()
    while row:
        if int(row[1]) in known or os.path.isfile('pos_training_files/%s.csv'%row[1]) or os.path.isfile('newtrainingsdata/%s.NEW.csv'%row[1]):
            if row[1] in known:
                print('K')
            row = cur.fetchone()
            continue
        
        TEST_DATA.append(row)
        row = cur.fetchone()
        

TAG_MAP_RAW = {
    'B': {'pos': 'ADJ','name':'BIOS', 'color':'bright_green'},
    'T': {'pos': 'ADP','name':'TITLE', 'color':'coral'},
    'H': {'pos': 'AUX','name':'HOUR', 'color':'yellow'},
    'C': {'pos': 'DET','name':'CITY', 'color':'dark_green'},
    'D': {'pos': 'NUM','name':'DAY', 'color':'sky_blue'},
    'TEL': {'pos': 'PART','name':'TELEPHONE', 'color':'light_green'},
    'S': {'pos': 'PROPN','name':'STREET', 'color':'light_orange'},
    'X': {'pos': 'X','name':'CRUFT', 'color':'red'},
    'L': {'pos': 'INTJ','name': 'AGE', 'color':'dark_teal'},
    'E': {'pos': 'VERB','name':'END_MOVIES', 'color':'grey50'},
    'P': {'pos':'PUNCT', 'name':'SEPARATOR', 'color':'light_turquoise'},
    'SS': {'pos': 'PRON', 'name': 'SUBSECTION', 'color':'pink'},
    'ST': {'pos': 'SYM','name':'START_MOVIES', 'color':'gray80'},
    '_SP': {'pos':'SPACE','name':'SPACE','color':'aqua'},
    'RM': {'pos':'PART','name':'remark','color':'ocean_blue'},
    'RB': {'pos':'PART','name':'remark','color':'lavender'},#NOT USING THIS ONE??
    '':{'pos':'SCONJ','name':'??????','color':'red'},
    'MD':{'pos':'ADV','name':'???????','color':'red'},
    'PRP':{'pos':'CONJ','name':'??????????','color':'red'},
    'VBP':{'pos':'CONJ','name':'??????????','color':'red'},
    'VB':{'pos':'VERB','name':'??????????','color':'lime'}
}

lestyles={}
for k,v in TAG_MAP_RAW.items():
    lestyles[v['color']] = style = xlwt.easyxf('pattern: pattern solid, fore_colour %s' % v['color'])

PATLEN=4
TAG_MAP={}
for k,v in TAG_MAP_RAW.items():
    TAG_MAP[k] = {'pos': v['pos']}

TRAIN_DATA = []
ct=0
taglist={}
dosave=True
known=[]

patterns = {}
pattern = []
wordmap={}
for x in os.listdir('pos_training_files'):
    if x.startswith('.'):
        continue
    ct+=1
    with codecs.open('pos_training_files/%s' % x, 'r', encoding='utf-8') as f:
        r=csv.reader(f, delimiter=';')
        stri=[]
        tags=[]
        for row in r:
            
            if row[1] in ['0','',' ']:
                ta = 'X'
            else:
                ta=row[1].upper()
            stri.append(row[0])
            tags.append(ta)
            try:
                taglist[ta]
            except KeyError:
                taglist[ta]={}
            try:
                wordmap[row[0]]
            except KeyError:
                wordmap[row[0]]={}
            try:
                wordmap[row[0]][ta]+=1
            except KeyError:
                wordmap[row[0]][ta]=1
            try:
                taglist[ta][row[0]].append(ta)
            except KeyError:
                taglist[ta][row[0]] = [ta]
            pattern.append("%s@%s"%(row[0],ta))
            if len(pattern) > PATLEN:
                pattern=pattern[1:]
                curpat = "&".join(pattern)
                try:
                    patterns[curpat]+=1
                except KeyError:
                    patterns[curpat]=1
        for tag in tags:
            if not tag in TAG_MAP.keys():
                print(x, tag)
        newtags = []
        for i, t in enumerate(tags):
            nt=t
            try:
                if t in ['P','X',''] and tags[i-1] == tags[i+1] and not tags[i-1] in['X','P']:
                    nt = tags[i+1]
                if t == 'L' and tags[i-1] == tags[i+1] and tags[i+1] =='H':
                    nt='H'
            except IndexError:
                pass
            if nt in ['RB','MD','PRP','VBP','']:
                t='X'
            newtags.append(nt)
        TRAIN_DATA.append((" ".join(stri), {'tags': newtags}))

print(ct, 'train files')

@plac.annotations(
    lang=("ISO Code of language to use", "option", "l", str),
    output_dir=("Optional output directory", "option", "o", Path),
    n_iter=("Number of training iterations", "option", "n", int))

def main(lang='en', output_dir=None, n_iter=25):
    """Create a new model, set up the pipeline and train the tagger. In order to
    train the tagger with a custom tag map, we're creating a new Language
    instance with a custom vocab.
    """
    nlp = spacy.blank(lang)
    if False:
        # add the tagger to the pipeline
        # nlp.create_pipe works for built-ins that are registered with spaCy
        tagger = nlp.create_pipe('tagger')
        # Add the tags. This needs to be done before you start training.
        for tag, values in TAG_MAP.items():
            tagger.add_label(tag, values)
        nlp.add_pipe(tagger)

        optimizer = nlp.begin_training()
        for i in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(TRAIN_DATA, size=compounding(4., 32., 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, losses=losses)
            print(i, 'Losses', losses)
    else:
        nlp = spacy.load('model')
    
    stylewarn = xlwt.easyxf('font: colour red;')
    styleok = xlwt.easyxf('font: colour green;')
    style=None
    ya=0
    for row in TEST_DATA:
        doc = nlp(row[0])
        curpat = []
        out=[]
        for i, t in enumerate(doc):
            out.append([t.text, t.tag_,0])
            curpat.append("%s@%s"%(t.text,t.tag_))
            while len(curpat) > PATLEN:
                curpat=curpat[1:]
            try:
                if patterns['&'.join(curpat)] > 0:
                    
                    for j in range(max(0,i-PATLEN),i):
                        out[j][2]=patterns['&'.join(curpat)]
            except KeyError:
                pass
            ya += 1
        sheet = book.add_sheet(str(row[1]))
        idx=0
        cur=0
        for zz in out:
            style = lestyles[TAG_MAP_RAW[zz[1]]['color']]
            sheet.write(idx, 0, zz[0], style)
            sheet.write(idx, 1, zz[1], style)
            idx+=1
    book.save('to_correct.xls')
    output_dir='model'
    
    if dosave:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

 

if __name__ == '__main__':
    plac.call(main)