from itertools import chain
from fuzzywuzzy import fuzz
from collections import Counter
import joblib, sys, pickle
import sklearn, re
import scipy.stats
from random import shuffle
from sklearn.metrics import make_scorer
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RandomizedSearchCV
from cityyearclass import Narrower



import sklearn_crfsuite, codecs, csv
from sklearn_crfsuite import scorers
from sklearn_crfsuite import metrics

filename = "extra-data/model.model"

def print_state_features(state_features):
    for (attr, label), weight in state_features:
        print("%0.6f %-8s %s" % (weight, label, attr))
def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]
    postag2 = ''
    try:
        wmin3=word[-2:]
    except IndexError:
        wmin3=''
    hwlen=int(len(word)/2)
    sw=word[:hwlen]
    ew=word[hwlen:]
    try:
        ups = word[0].isupper()
    except IndexError:
        ups = False

    features = {
        'bias': 1.0,
        'word': word,
        'word.st': word.lower()[:4],
        'word.e': word.lower()[-4:],
        'word[-2:]': wmin3,
        'sw': sw,
        'ew': ew,
        'wu': ups,
        'word[-1:]': word[-1:],
        'word[-2:]': word[-2:],
        'word[-4:]': word[-4:],
        'wlen': len(word),
        'word.isdigit()': word.isdigit(),
        'postag': postag
        
    }
    for ii in range(-2,3):
        if ii == 0:
            continue
        try:
            word1 = sent[ii+i][0]
        except IndexError:
            continue
        postag1 = sent[ii+i][1]
        postag12 = ''
        hwlen=int(len(word1)/2)
        sw=word1[:hwlen]
        ew=word1[hwlen:]
        try:
            ups = word1[0].isupper()
        except IndexError:
            ups = False
        features.update({
            '%s:word' % ii : word1,
            '%s:[:3]' % ii: word1[:3],
            '%s:[-4]': word1[-4:],
            '%s:[-2]': word1[-2:],
            '%s:sw' % ii: sw,
            '%s:ew' % ii: ew,
            '%s:wu': ups,
            '%s:word' % ii: word1,
            '%s:wlen' % ii: len(word1),
            '%s:isdigit()' % ii: word1.isdigit(),
            '%s:postag' % ii: postag1
        })
            
    
    if i == 1:
        features['BOS'] = True
    
    if i == len(sent)-1:
        features['EOS'] = True

    return features


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [label for token, postag, label in sent]

def sent2tokens(sent):
    return [token for token, postag, label in sent]




sents=[]

ss=True
for i in range(0,4):
    cursent=[]
    try:
        with codecs.open('crftrain%s.csv' % str(i),'r', encoding='utf-8') as f:
            r=csv.reader(f, delimiter="\t")
            for row in r:
                if not row[0].strip():
                    ss=True
                    continue
                if ss:
                    sents.append(cursent)
                    cursent=[]
                    ss=False
                tag='NP'
                if row[1] == 'O-BP':
                    tag='O'
                cursent.append((row[0], row[2], row[1]))
    except Exception:
        pass
shuffle(sents)
half = int(len(sents)/7)
test_sents, train_sents = [sents[:half], sents[half:]]
print(len(train_sents), 'train', len(test_sents), 'test')

    
X_train = [sent2features(s) for s in train_sents]
y_train = [sent2labels(s) for s in train_sents]

X_test = [sent2features(s) for s in test_sents]
y_test = [sent2labels(s) for s in test_sents]
crf = sklearn_crfsuite.CRF(
    algorithm='lbfgs',
    c1=0.1,
    c2=0.1,
    max_iterations=100,
    all_possible_transitions=True
)
crf.fit(X_train, y_train)
y_pred = crf.predict(X_test)

labels = list(crf.classes_)

print(metrics.flat_f1_score(y_test, y_pred, average='weighted', labels=labels))
print("Top positive:")
try:
    print_state_features(Counter(crf.state_features_).most_common(30))
except Exception:
    pass

print("\nTop negative:")
try:
    print_state_features(Counter(crf.state_features_).most_common()[-30:])
except Exception:
    pass


sorted_labels = sorted(
    labels,
    key=lambda name: (name[1:], name[0])
)
print(metrics.flat_classification_report(
    y_test, y_pred, labels=sorted_labels, digits=3
))

joblib.dump(crf, filename, compress=9)
