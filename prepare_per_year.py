from parserclass import Movieparser
import os, pickle

if __name__ == '__main__':

    for year in range(1948, 1996):
        for city in ['amsterdam','rotterdam','utrecht','sgravenhage']:
            if os.path.isfile('%s/%s_%s.p' % (city, city, year)):
                continue
            print(city, year)
            n = Movieparser(city, year)
            
            found_dates = n.setfounddates()

            okdays = n.setokdays()
            oktitles, okppn= n.setoktitles()
            n.getmissingladders()
            print('Pass 1')
            miss=n.getmissingweeks()
            print(len(miss), miss)
            if len(miss) > 0:
                n.getmissingladders_pass_2()
                print('Pass 2')
                miss=n.getmissingweeks()
                print(len(miss), miss)
            n.conn.close()

            pickle.dump(n.datemap, open('%s/%s_%s.p' % (city, city, year), 'wb'))
            del n
        
  
