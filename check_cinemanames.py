import pickle, codecs, csv
from operator import itemgetter
from collections import Counter

dta = pickle.load(open('badcins.p','rb'))
c= Counter(dta)
out=[]
for k,v in sorted(c.most_common(40), key=itemgetter(0)):
    out.append([k,v])
with codecs.open('suspicious_cinemas.csv','w', encoding='utf-8') as f:
    w=csv.writer(f, delimiter=';')
    w.writerows(out)