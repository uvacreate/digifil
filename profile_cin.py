import csv, codecs, datetime
from collections import Counter
import numpy as np

ccmap={}
with codecs.open('extra-data/tblFilm.csv','r', encoding='utf-8') as f:
    r=csv.DictReader(f)
    for row in r:
        if row['imdb'].strip():
            ccmap[row['film_id']] = int(row['imdb'].replace('/','').replace('s',''))

            
            
ccmap[253436]=38823
            
x=1949
city='amsterdam'
m={}
tmap={}
with codecs.open('matches/%s/filtered/separated_titles/replaced_titles/matched/spliced/%s_%s.csv' % (city,city,x), 'r', encoding='utf-8') as f:
    r=csv.DictReader(f, delimiter=';')
    for row in r:
        if row['isok'] == '1':
            # print(row['cinema'], row['date'], row['match_id'], row['times'])
            try:
                m[row['cinema']]
            except KeyError:
                m[row['cinema']]={}
            if row['imdb'].strip():
                row['match_id'] = row['imdb']
            try:
                row['match_id'] = int(row['match_id'])
            except ValueError:
                pass
            try:
                row['match_id'] = ccmap[row['match_id']]
            except KeyError:
                pass
            try:
                row['match_id'] = int(row['match_id'])
            except ValueError:
                pass
            try:
                row['match_id'] = ccmap[row['match_id']]
            except KeyError:
                pass
            try:
                m[row['cinema']][row['date']].append([row['match_id'], row['times']])
            except KeyError:
                m[row['cinema']][row['date']]=[[row['match_id'], row['times']]]
            tmap[row['match_id']] = [row['title2'], row['title'], row['match']]
            tmap[row['match_id']] = row['match']
prev=None
switches=[]
for xx in m.keys():
    for k,v in sorted(m[xx].items()):
        
        dt = datetime.datetime.strptime(k, '%Y-%m-%d')
        if not v[0][0] == prev:
            prev=v[0][0]
            yesterday = dt - datetime.timedelta(days=-1)
            try:
                #Only add to switches if the previous day is also in the dataset
                m[xx][str(yesterday)[:10]]
                switches.append(dt.weekday())
            except KeyError:
                pass
    commonday = int(np.median(switches))
    
    #Try to find 'second median'
    moo = [o for o in switches if not o == commonday]
    second = int(np.median(moo))
    commonday1 = np.median(switches[:10])
    commonday2 = np.median(switches[-10:])

    weekitems=[]
    outy=[[''],[xx.upper()]]
    if not commonday1 == commonday2:
        outy.append(['COMMONDAY MISMATCH', commonday1, commonday2, np.std(switches)] )
        outy.append(['%s occurrences of day %s' % (switches.count(second), second)])
    weekstart=datetime.datetime.strptime('1949-01-01', '%Y-%m-%d')
    anyf=False
    for k,v in sorted(m[xx].items()):
        dt = datetime.datetime.strptime(k, '%Y-%m-%d')
        no_days = dt-weekstart
        no_days=no_days.days
        if dt.weekday() == commonday or no_days>=7:
            if not len(set(weekitems)) <2:
                outy.append(["","%s t/m %s" % (str(weekstart)[5:10],str(dt)[5:10])])
                for kk,gv in Counter(weekitems).items():
                    outy.append(["\t\t",str(kk).ljust(10),gv, tmap[kk]])
                weekstart=dt
                anyf=True
            weekitems=[]
        for vv in v:
            weekitems.append(vv[0])
    if anyf:
        for zz in outy:
            print("\t".join([str(z) for z in zz]))
print('.')