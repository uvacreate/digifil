import csv, codecs, os, pickle, psycopg2
from random import choice, randint
import spacy
nlp = spacy.load('en_core_web_sm')

extra=['1','2','3','4','5','6','7','8','i','ii','iii','iv','v','vi','vii','viii','ix','x']

tits2 =  pickle.load(open('crftitles.p','rb'))

sents=[]
alltitles=[]

for i in range(0,1000000):
    no_cins = randint(1,12)
    sent = ""
    titles=[]
    if i % 100 == True:
        print(i)
    for cinno in range(0,no_cins):
        tit = choice(tits2)
        taggedtit=[]
        splittit=tit.split(' ')
        for j, t in enumerate(splittit):
            
            tag = 'NP'
            if j == 0:
                tag = 'B-NP'
            elif j == len(splittit)-1:
                tag = 'E-NP'

            taggedtit = (t, tag, '')
            titles.append(taggedtit)
        if False:
            doc=nlp(tit)
            for j, entity in enumerate(doc):
                t,pos = entity.text, entity.pos_
                tag = 'NP'
                if j == 0:
                    tag = 'B-NP'
                elif j == len(doc)-1:
                    tag = 'E-NP'
                
                taggedtit = (t, tag, pos)
                titles.append(taggedtit)
                
        if randint(0,9) == 1 and not cinno == no_cins-1:
            ggg=choice(extra)
            titles.append((ggg, 'O-BP', 'O'))

    alltitles += titles
    alltitles.append(('',''))
with codecs.open('crftrain.csv','w', encoding='utf-8') as f:
    w=csv.writer(f, delimiter="\t")
    w.writerows(alltitles)