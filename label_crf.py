import spacy, os
nlp = spacy.load('en_core_web_sm')
import csv, codecs, pickle

for wantyear in [1994]:
    for city in os.listdir('matches'):
        for x in os.listdir('matches/%s/filtered' % city):
            year = x.replace(city,'').replace('_','').replace('.csv','')
            if not str(wantyear) in x:
                continue
            sents=[]
            ct=0
            print(city, year)
            if x.endswith('.csv') and not x.startswith('.'):
            
                with codecs.open('matches/%s/filtered/%s' % (city, x), 'r', encoding='utf-8') as f:
                    r=csv.DictReader(f, delimiter=';')
                    cursent=[]
                    for row in r:
                        ct+=1
                        
                        tit = row['title']
                        doc=nlp(tit)
                        titlebits=[]
                        ss=True
                        for j, entity in enumerate(doc):
                            t,pos = entity.text, entity.pos_
                            tag = 'NP'
                            if j == 0:
                                tag = 'B-NP'
                            elif j == len(doc)-1:
                                tag = 'E-NP'
                            
                            taggedtit = (t, tag, pos)
                            titlebits.append(taggedtit)

                        added=False
                        for rrow in titlebits:
                            if not rrow[0].strip():
                                ss=True
                                continue
                            tag='NP'
                            if rrow[1] == 'O-BP':
                                tag='O'
                            cursent.append((rrow[0], rrow[2], rrow[1],ct))
                        if not added:
                            sents.append(cursent)
                        cursent=[]

                pickle.dump(sents, open('matches/%s/filtered/separated_titles/%s_%s.p' % (city, city, year),'wb'))
                print(len(sents))