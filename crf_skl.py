from itertools import chain
from fuzzywuzzy import fuzz
from collections import Counter
import joblib, sys, pickle
import sklearn, re, os
import scipy.stats
from random import shuffle
from sklearn.metrics import make_scorer
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RandomizedSearchCV
from cityyearclass import Narrower



import sklearn_crfsuite, codecs, csv
from sklearn_crfsuite import scorers
from sklearn_crfsuite import metrics

filename = "model.model"

def print_state_features(state_features):
    for (attr, label), weight in state_features:
        print("%0.6f %-8s %s" % (weight, label, attr))
def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]
    postag2 = ''
    try:
        wmin3=word[-2:]
    except IndexError:
        wmin3=''
    hwlen=int(len(word)/2)
    sw=word[:hwlen]
    ew=word[hwlen:]
    try:
        ups = word[0].isupper()
    except IndexError:
        ups = False

    features = {
        'bias': 1.0,
        'word': word,
        'word.st': word.lower()[:4],
        'word.e': word.lower()[-4:],
        'word[-2:]': wmin3,
        'sw': sw,
        'ew': ew,
        'wu': ups,
        'word[-1:]': word[-1:],
        'word[-2:]': word[-2:],
        'word[-4:]': word[-4:],
        'wlen': len(word),
        'word.isdigit()': word.isdigit(),
        'postag': postag
        
    }
    for ii in range(-2,3):
        if ii == 0:
            continue
        try:
            word1 = sent[ii+i][0]
        except IndexError:
            continue
        postag1 = sent[ii+i][1]
        postag12 = ''
        hwlen=int(len(word1)/2)
        sw=word1[:hwlen]
        ew=word1[hwlen:]
        try:
            ups = word1[0].isupper()
        except IndexError:
            ups = False
        features.update({
            '%s:word' % ii : word1,
            '%s:[:3]' % ii: word1[:3],
            '%s:[-4]': word1[-4:],
            '%s:[-2]': word1[-2:],
            '%s:sw' % ii: sw,
            '%s:ew' % ii: ew,
            '%s:wu': ups,
            '%s:word' % ii: word1,
            '%s:wlen' % ii: len(word1),
            '%s:isdigit()' % ii: word1.isdigit(),
            '%s:postag' % ii: postag1
        })
            

    if i == 1:
        features['BOS'] = True
    

    if i == len(sent)-1:
        features['EOS'] = True

    return features


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def sent2labels(sent):
    return [label for token, postag, label in sent]

def sent2tokens(sent):
    return [token for token, postag, label in sent]

def getcur(cur):
    ccur= " ".join(cur).strip()
    cur2 = 'noexiste' 
    cucur=ccur.replace(' ','_')
    repld=False
    if re.search(' [0-9]$', ccur):
        cur2 = cucur[:-2].strip()
    try:
        corrcin = variants[cur2]
        ccur = [corrcin]
        repld=True
    except KeyError:
        try:
            corrcin = variants[cur2.replace('_',' ')]
            ccur = corrcin
            repld=True
        except KeyError:
            pass
        pass
    if not repld:
        test = ccur.split(' ')
        try:
            lastword = test[-1]
            ccur = [" ".join(test[:-1]), variants[test[-1]]]
            repld=True
        except KeyError:
            pass
        try:
            firstword = test[0]
            ccur = [variants[firstword], " ".join(test[1:])]
            repld=True
        except KeyError:
            pass
    
        
    return ccur


crf = joblib.load('../split_cities/%s' % filename)
todo = []
year=1994

for city in ['amsterdam','rotterdam','sgravenhage','utrecht']:
    for x in os.listdir('matches/%s/filtered/separated_titles' % city):
        if x.startswith('.') or 'lock'  in x or not x.endswith('.p'):
            continue
        if not str(year) in x:
            continue
        test_sents = pickle.load(open('matches/%s/filtered/separated_titles/%s' % (city,x),'rb'))
        print(x)
        X_test = [sent2features(s) for s in test_sents]
        y_pred = crf.predict(X_test)
        nar = Narrower(city, year)
        nar.load_tits()
        addtits = nar.tits
        with codecs.open('../split_cities/additional_titles.csv','r', encoding='utf-8') as f:
            r=csv.reader(f, delimiter=';')
            for row in r:
                try:
                    addtits[row[0]] = row[1]
                except IndexError:
                    pass
        variants = nar.variants
        for k,v in dict(variants).items():
            variants[k.replace(' ','_')] = v
        
        out=[]
        vars=[]
        for q in nar.variants:
            vars.append(q.lower())
        vars=set(vars)

        ct=0
        restofdata={}
        with codecs.open('matches/%s/filtered/%s' % (city,x.replace('.p','.csv')), 'r', encoding='utf-8') as f:
            r=csv.DictReader(f, delimiter=';')
            cursent=[]
            for row in r:
                row['iiiid'] = ct
                restofdata[ct] = row
                ct+=1
        aligner=0
        curmeta=None
        moo = None
        yout=[]
        print(len(y_pred), len(restofdata))
        for j, y in enumerate(y_pred):
            if not moo:
                moo=j
            cur=[]
            walls=[]
            wall=[]
            for jj, m in enumerate(test_sents[j]):
                if y[jj].startswith('B-'):
                    if not cur == []:
                        opp = getcur(cur)
                        if type(opp) == list:
                            walls += opp[0]
                            wall = []
                            wall = [opp]
                        else:
                            wall.append(opp)
                    cur=[]
                    cur.append(m[0])

                elif y[jj].startswith('E-'):
                    cur.append(m[0])
                    opp = getcur(cur)
                    if type(opp) == list:
                        wall += opp
                    else:
                        wall.append(opp)

                    cur=[]
                else:
                    cur.append(m[0])
            if not cur == []:
                opp = getcur(cur)
                if type(opp) == list:
                    wall += opp
                else:
                    wall.append(opp)
            

            dd = restofdata[moo]
            
            for w in wall:
                ddd= dd.copy()
                ddd['title2'] = w
                yout.append(ddd)
                keys=list(ddd.keys())
            moo+=1
        with codecs.open('matches/%s/filtered/separated_titles/replaced_titles/%s' % (city,x.replace('.p','.csv')), 'w', encoding='utf-8') as f:
            w=csv.DictWriter(f, delimiter=';', fieldnames=keys)
            w.writeheader()
            w.writerows(yout)
