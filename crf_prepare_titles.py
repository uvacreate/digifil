import pickle, psycopg2
from random import choice, randint

extra=['1','2','3','4','5','6','7','8','i','ii','iii','iv','v','vi','vii','viii','ix','x']

def isLatin(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True

conn=psycopg2.connect(database="film", user="postgres", password="root",port=5432)
conn.set_client_encoding('UTF8')
cur = conn.cursor()
cur.execute("select title from imdb_titles")
row=cur.fetchone()
titles=[]
ct=0
while row:
    if isLatin(row[0]):
        titles.append(row[0])
    ct+=1
    if ct % 100000 == True:
        print(ct)
    row=cur.fetchone()

cur.execute("select title from cc_titles")
row=cur.fetchone()

ct=0
while row:
    titles.append(row[0])
    ct+=1
    if ct % 100000 == True:
        print(ct)
    row=cur.fetchone()

pickle.dump(titles, open('crftitles.p','wb'))