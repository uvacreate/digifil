import os, csv, codecs, datetime, sys, psycopg2
from fuzzywuzzy import fuzz
from collections import Counter
from cityyearclass import Narrower
conn=psycopg2.connect(database="delpher", user="postgres", password="root",port=5432)
conn.set_client_encoding('UTF8')
cur = conn.cursor()
repl={}
bad=[]

year = 1993

days=['maandag','dinsdag','woensdag','donderdag','vrijdag','zaterdag','zondag','pinksterdagen','kerstdagen','kerstd','paasdagen','doorlopend','dagelijks','doorl']
streets=[]
with codecs.open('../split_cities/streets.csv'  ,'r', encoding='utf-8') as f:
    r=csv.reader(f,delimiter=';')
    for row in r:
        streets.append(row[0])

with codecs.open('matchmap/check/%s.csv' % year ,'r', encoding='utf-8') as f:
    r=csv.DictReader(f,delimiter=';')
    for row in r:
        if row['isok'] == 'x':
            bad.append(row['part_title'])
        elif row['isok'] == '1':
            repl[row['part_title']] = [row['rawid'], row['imdbid']]
tits=None

def cl(s):
    if len(s) < 1:
        return s
    s=s.replace(' , ',', ')
    s=s.replace(' . ','. ')
    while len(s) > 1 and not s[-1].isalnum():
        s=s[:-1].strip()
        
    while len(s) > 1 and not s[0].isalnum():
        s=s[1:].strip()
    return s
for city in ['utrecht','sgravenhage','rotterdam','amsterdam']:
    print(city)
    nar = Narrower(city, year)
    if not tits:
        nar.load_tits()
        tits = nar.tits
        with codecs.open('../split_cities/additional_titles.csv','r', encoding='utf-8') as f:
            r=csv.reader(f, delimiter=';')
            for row in r:
                tits[row[0]] = row[1]
    
    if not os.path.isdir('matches/%s/filtered/separated_titles/replaced_titles/matched/spliced' % city):
        os.makedirs('matches/%s/filtered/separated_titles/replaced_titles/matched/spliced' % city)

    article_cinemacheck = {}
    with codecs.open('matches/%s/filtered/separated_titles/replaced_titles/matched/%s_%s.csv' % (city,city,year),'r', encoding='utf-8') as f:
        r=csv.DictReader(f,delimiter=';')
        out=[]
        sorter=0
        m={}
        cinecount=[]
        for row in r:
            sorter+=1
            if row['title2'] in bad:
                continue
            
            if not row['title2'] or not cl(row['title2']):
                continue
            row['sorter'] = sorter
            row['isok'] = None
            cinecount.append(row['cinema'])
            if row['cinema'] in nar.cinemas_this_city:
                try:
                    re = repl[row['title2']]
                    row['match_id'] = re[0]
                    row['imdb'] = re[1]
                    row['isok']=1
                except KeyError:
                    pass
                    
                #Let's trust very high match ratios that weren't split up
                if row['title'] == row['title2'] and int(row['ratio']) > 190:
                    row['isok'] = 1
                if row['isok'] == 1:
                    try:
                        m[row['cinema']]
                    except KeyError:
                        m[row['cinema']]={}
                    try:
                        m[row['cinema']][row['date']].append(row)
                    except KeyError:
                        m[row['cinema']][row['date']]= [row]
            else:
                row['isok']='x'

            if row['isok'] in [1,None,'1',''] and row['cinema'] in nar.cinemas_this_city:
                try:
                    article_cinemacheck[int(row['article_id'])].append(row['cinema'])
                    article_cinemacheck[int(row['article_id'])]=list(set(article_cinemacheck[row['article_id']]))
                except KeyError:
                    article_cinemacheck[int(row['article_id'])] =[row['cinema']]
                    
            if row['cinema'] in nar.unique_to_city:
                try:
                    article_cinemacheck[int(row['article_id'])].append('OK')
                except KeyError:
                    article_cinemacheck[int(row['article_id'])] = ['OK']
            row['title2'] = cl(row['title2'])
            lumpkey = "%s_%s" % (row['article_id'], row['cinema'])
            try:
                lumper[lumpkey].append(row['title2'])
            except KeyError:
                lumper[lumpkey] = [row['title2']]
            out.append(row)

    okd=0
    newout=[]
    cinecount=Counter(cinecount)
    lumpreplaced=[]
    lumped=[]
    tot=len(out)
    ct=0
    for row in out:
        ct+=1
        if ct % 100 == True:
            print(ct, '/',tot)
        if not row['title2'] or not row['title2'].strip():
            continue
        if not row['isok']:
            trytit = row['title2'].lower()
            if not trytit:
                continue
            top=0
            for d in days:
                rat=fuzz.ratio(d.lower(), trytit)
                if rat > top:
                    top=rat
            for d in streets:
                rat=fuzz.ratio(d.lower(), trytit)
                if rat > top:
                    top=rat
            if top > 90:
                row['isok'] = 'x'
            else:
                if cinecount[row['cinema']] < 10:
                    row['isok'] = 'XXX'
                else:
                    if row['cinema'] in nar.cinemas_this_city:
                        #Find the same title within 14 days in the same cinema
                        base = datetime.datetime.strptime(row['date'], '%Y-%m-%d')
                        date_list = [base - datetime.timedelta(days=x) for x in range(0, 7)]
                        date_list += [base + datetime.timedelta(days=x) for x in range(0, 7)]
                        foundany=False
                        foundimdbs=[]
                        for d in date_list:
                            if d == base:
                                continue
                            try:
                                for qq in m[row['cinema']][str(d)[:10]]:
                                    rat = fuzz.ratio(row['title2'], qq['title2'])
                                    
                                    if rat > 70:
                                        foundany=True
                                        foundimdbs=[row['imdb'], row['match_id']]
                                        break
                            except KeyError:
                                pass
                        if foundany:
                            row['isok'] = 1
                            row['imdb'] = foundimdbs[0]
                            row['match_id'] = foundimdbs[1]
                            okd+=1
                        else:
                            lumpkey = "%s_%s" % (row['article_id'], row['cinema'])
                            if lumpkey in lumped:
                                row['isok'] = 'LUMPED'
                            else:
                                fnd=None
                                top=0
                                if len(lumper[lumpkey]) == 2:
                                    trytitle = cl(" ".join(lumper[lumpkey]))
                                    if not trytitle:
                                        continue
                                    for t in tits:
                                        if top > 99:
                                            break
                                        rat = fuzz.ratio(t, trytitle.lower())
                                        if rat > 90 and rat>top:
                                            top=rat
                                            fnd=[t,tits[t]]
                                    if fnd:
                                        row['match'] = fnd[0]
                                        row['match_id'] = fnd[1]
                                        row['imdb'] = fnd[1]
                                        row['isok'] = 1
                                        lumped.append(lumpkey)
        try:
            if len(article_cinemacheck[int(row['article_id'])]) > 3 or 'OK' in article_cinemacheck[int(row['article_id'])]:
                newout.append(row)
                
        except KeyError:
            row['city_ok'] = 0
    out = sorted(newout, key=lambda k: (k['cinema'],k['date'],k['iiiid'])) 
    with codecs.open('matches/%s/filtered/separated_titles/replaced_titles/matched/spliced/%s_%s.csv' % (city,city,year),'w', encoding='utf-8') as f:
        w=csv.DictWriter(f, delimiter=';', fieldnames=['city_ok','isok','article_id','date','cinema','iiiid','title2','imdb','match','title','match_id','startposition','ratio','url','iets','times','ages','weekno','sorter'])
        w.writeheader()
        w.writerows(out)
        
