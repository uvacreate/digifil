# from cityyearclass import Narrower
import sys,pickle, datetime
import numpy as np

from fuzzywuzzy import fuzz
from multiprocessing import Process, Queue, current_process, freeze_support
import os, codecs, csv

ccmap={}
with codecs.open('tblFilm.csv','r', encoding='utf-8') as f:
    r=csv.DictReader(f)
    for row in r:
        if row['imdb'].strip():
            ccmap[row['film_id']] = int(row['imdb'].replace('/','').replace('s',''))

def worker(input, output, kt):
    knowntitles=set(kt.keys())
    for title in iter(input.get, 'STOP'):
        ret = [0,title,'','']
        top=0
        for t in knowntitles:
            if ret[0] == 200:
                continue
            if len(set(title).intersection(set(t))) > 2:
                rat = fuzz.ratio(t.lower(), title.lower()) + fuzz.token_set_ratio(t.lower(), title.lower())
                if rat>ret[0]:
                    ret=[rat, title, t, kt[t]]
        
        output.put(ret)

def cl(s):
    s=s.replace(' , ',', ')
    s=s.replace(' . ','. ')
    try:
        while not s[-1].isalnum():
            s=s[:-1].strip()
    except IndexError:
        pass
    try:
        while not s[0].isalnum():
            s=s[1:].strip()
    except IndexError:
        pass
    return s
if __name__ == '__main__':
    cities = ['utrecht','amsterdam','rotterdam','sgravenhage']
    NUMBER_OF_PROCESSES = 8
    year = 1959
    
    step=20
    titles = []
    kt=pickle.load(open('titcache/tits_latin_%s.p'%year, 'rb'))

    with codecs.open('extra-data/additional_titles.csv','r', encoding='utf-8') as f:
        r=csv.reader(f, delimiter=';')
        for row in r:
            kt[row[0]] = row[1]
    if os.path.isfile('matchmap/%s.p' % year):
        allfound=pickle.load(open('matchmap/%s.p' % year, 'rb'))
    else:
        for city in cities:
            try:
                for x in os.listdir('matches/%s/filtered/separated_titles/replaced_titles' % city):
                    if x.endswith('.csv') and not x.startswith('.') and str(year) in x:
                        print('Processing',x)
                        with codecs.open('matches/%s/filtered/separated_titles/replaced_titles/%s' % (city, x), 'r', encoding='utf-8') as f:
                            r=csv.DictReader(f, delimiter=';')
                            for row in r:
                                if row['title2'].strip():
                                    for tit in row['title2'].split('/'):
                                        tit=cl(tit)
                                        try:
                                            newtit = cl(row['title2']).lower()
                                        except IndexError:
                                            continue
                                        titles.append(newtit)
                                    
            except Exception as ee:
                print(ee)
                sys.exit('Not all present yet!')
        print(len(set(titles)), len(titles))
        allfound={}
        todo=[]
        for t in set(titles):
            found=False
            try:
                allfound[t]=[400,t ,kt[t]]
            except KeyError:
                todo.append(t)
        print(len(allfound), len(todo))
        
        task_queue = Queue()
        done_queue = Queue()
        for task in todo:
            task_queue.put(task)
        for i in range(NUMBER_OF_PROCESSES):
            print('Start',i)
            Process(target=worker, args=(task_queue, done_queue, kt)).start()
        yact=0
        t1 = datetime.datetime.now()
        times=[]
        for i in range(len(todo)):
            if i % 100==True:
                t2 = datetime.datetime.now()
                delta = t2 - t1
                t1=t2
                times.append(delta.seconds)
                remaining=1
                remaining = ((len(todo)-i)/step)*(np.mean(times)/60)
                # print(i, len(todo), year)
                print("%s/%s done" % (i, len(todo)), "%.1f" % (delta.seconds/60), 'min, ETA', "%.1f" % (remaining),"(%.1f h)" % (remaining/60), year)
            res = done_queue.get()
            allfound[res[1]] = [res[0], res[2], res[3]]
            if i % 500==True:
                pickle.dump(allfound, open('tempcache.p','wb'))
        for i in range(NUMBER_OF_PROCESSES):
            task_queue.put('STOP')
        pickle.dump(allfound, open('matchmap/%s.p' % year, 'wb'))
    allfound['']=['','','']
    for city in cities:
        for x in os.listdir('matches/%s/filtered/separated_titles/replaced_titles' % city):
            if x.endswith('.csv') and not x.startswith('.') and str(year) in x:
                print('Matching',x)
                out=[]
                with codecs.open('matches/%s/filtered/separated_titles/replaced_titles/%s' % (city, x), 'r', encoding='utf-8') as f:
                    r=csv.DictReader(f, delimiter=';')
                    for row in r:
                        if row['title2'].strip():
                            # titles.append(row['title2'].lower())
                            for tit in row['title2'].split('/'):
                                tit=cl(tit).strip()
                                rrow = row.copy()
                                try:
                                    allfound[cl(tit).lower()]
                                except KeyError:
                                    ret=[0,'','','']
                                    print('Lookup', tit)
                                    for t in kt:
                                        if len(set(tit).intersection(set(t))) > 2:
                                            rat = fuzz.ratio(t.lower(), tit.lower()) + fuzz.token_set_ratio(t.lower(), tit.lower())
                                            if rat>ret[0]:
                                                ret=[rat, tit, t, kt[t]]
                                    allfound[cl(tit).lower()] = ret
                                rrow['ratio'] = allfound[cl(tit).lower()][0]
                                rrow['match'] = allfound[cl(tit).lower()][1]
                                rrow['match_id'] = allfound[cl(tit).lower()][2]
                                try:
                                    rrow['imdb'] = int(rrow['match_id'])#Imdb ID
                                except ValueError:
                                    try:
                                        rrow['imdb'] = ccmap[rrow['match_id']]
                                    except KeyError as err:
                                        # print('err',err)
                                        pass
                                rrow['title2']=tit
                                out.append(rrow)
                pickle.dump(allfound, open('matchmap/%s.p' % year, 'wb'))
                keys = ['weekno','date','cinema','title','title2','ratio','match','imdb','match_id','url','article_id','iets','times','ages','startposition','iiiid','city_ok']
                with codecs.open('matches/%s/filtered/separated_titles/replaced_titles/matched/%s' % (city,x), 'w', encoding='utf-8') as f:
                    w=csv.DictWriter(f, delimiter=';', fieldnames=keys)
                    w.writeheader()
                    w.writerows(out)
                print("Wrote", city, x)
    