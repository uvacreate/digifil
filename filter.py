import os, codecs, csv, pickle, re
import numpy as np
from cityyearclass import Narrower

confusion = pickle.load(open('extra-data/confusing_cinematitles.p','rb'))
cmap={}
for kk in confusion:
    for k in kk:
        for x in k[1:]:
            try:
                cmap[k[0]].append(x)
            except KeyError:
                cmap[k[0]] = [x]

def fix(s):
    s=s.replace(":'",": ")
    s=s.replace(":.",": ")
    s=s.replace(":,",": ")
    if s.endswith(','):
        s=s[:-1].strip()
    s = " ".join(s.split())
    return s.strip()
    
def fixtitle(splitt, row):
    fout=[]
    tit=[]
    rrow = row.copy()
    for j,x in enumerate(splitt):
        donotsplit=False
        if x.lower() in vars:
            try:
                for m in cmap[x.lower()]:
                    if m in row['title'].lower():
                        donotsplit=True
            except KeyError:
                pass
            if not donotsplit:
                rrow = row.copy()
                rrow['title'] = " ".join(tit)
                rrow['cinema'] = nar.variants[x.lower()]
                if not re.search(' [a-z]+ [a-z]+ (, )?[a-z]+ [a-z]+ [a-z]+', rrow['title'].lower()):
                    ok=True
                    for m in bads:
                        if m in rrow['title'].lower():
                            ok=False
                    if ok:
                        if rrow['title'].strip() and rrow['cinema'].strip():
                            fout.append(rrow)
                tit=[]
        elif j < len(splitt)-1 and "%s_%s" % (x.lower(), splitt[j+1].lower()) in vars:
            lekey="%s_%s" % (x.lower(), splitt[j+1].lower())
            try:
                for m in cmap[lekey]:
                    if m in row['title'].lower():
                        donotsplit=True
            except KeyError:
                pass
            # print("MISS 2", lekey, '>>',nar.variants[lekey])
            if not donotsplit:
                rrow = row.copy()
                rrow['title'] = " ".join(tit)
                rrow['cinema'] = nar.variants[lekey]
                if not re.search(' [a-z]+ [a-z]+ (, )?[a-z]+ [a-z]+ [a-z]+', rrow['title'].lower()):
                    ok=True
                    for m in bads:
                        if m in rrow['title'].lower():
                            ok=False
                    if ok:
                        if rrow['title'].strip() and rrow['cinema'].strip():
                            fout.append(rrow)
                tit=[]
        elif j < len(splitt)-2 and "%s_%s_%s" % (x.lower(), splitt[j+1].lower(), splitt[j+2].lower()) in vars:
            lekey="%s_%s_%s" % (x.lower(), splitt[j+1].lower(), splitt[j+2].lower())
            try:
                for m in cmap[lekey]:
                    if m in row['title'].lower():
                        donotsplit=True
            except KeyError:
                pass
            if not donotsplit:
                
                rrow['title'] = " ".join(tit)
                rrow['cinema'] = nar.variants[lekey]
                if not re.search(' [a-z]+ [a-z]+ (, )?[a-z]+ [a-z]+ [a-z]+', rrow['title'].lower()):
                    ok=True
                    for m in bads:
                        if m in rrow['title'].lower():
                            ok=False
                    if ok:
                        if rrow['title'].strip() and rrow['cinema'].strip():
                            fout.append(rrow)
                tit=[]
            else:
                fout.append(rrow)
        else:
            tit.append(x)
    row['title'] = " ".join(tit)
    if row['title'].strip():
        if not re.search(' [a-z]+ [a-z]+ [a-z]+ [a-z]+ [a-z]+', row['title'].lower()):
            ok=True
            for m in bads:
                if m in row['title'].lower() and rrow['cinema'].strip():
                    ok=False
            if ok:
                fout.append(row)
            
    return fout
bads=['cabaret','ballet','concert','toneel','orkest','revue', 'tentoonstell', 'schilderij','orkest','dansinst','orgel']

for yr in [1994]:
    for city in ['amsterdam','utrecht','sgravenhage','rotterdam']:
        xx = 'matches/%s/%s_%s.csv' % (city, city, yr)
        xx = '%s_%s.csv' % (city, yr)
        article_cinemacheck={}
        print(xx)
        nar = Narrower(city, yr)
        nar.setrepl()
        vars = set(nar.variants.keys())
        wanted_cinemas = nar.cinemas_this_city + ['1','2','3','4','5','6','7','8','9','i','ii','iii','iv','v','vi','vii','viii','ix','x','xi','zaal','zaal 1','zaal 2']
        fout=[]
        out=[]
        okmap={}
        with codecs.open('matches/%s/%s' % (city,xx), 'r', encoding='utf-8') as f:
            r=csv.DictReader(f, delimiter=';')
            for row in r:
                row['title']=fix(row['title'])
                splitt=row['title'].split(' ')
                #Separate cinema names, taking into account names that can appear in titles
                try:
                    row['cinema'] = nar.variants[row['cinema'].lower()]
                except KeyError:
                    pass
                fout.append(row)
                if row['cinema'] in nar.cinemas_this_city:
                    try:
                        article_cinemacheck[int(row['article_id'])].append(row['cinema'])
                    except KeyError as rr:
                        article_cinemacheck[int(row['article_id'])] =[row['cinema']]    
                if row['cinema'] in nar.unique_to_city:
                    try:
                        article_cinemacheck[int(row['article_id'])].append('OK')
                    except KeyError:
                        article_cinemacheck[int(row['article_id'])] = ['OK']      

        for row in fout:
        
            if not row['cinema'].lower() in wanted_cinemas:
                continue
            if row['cinema'] in nar.cinemas_this_city:
                #we want at least two real cinemas found
                try:
                    okmap[row['article_id']] += 1
                except KeyError:
                    okmap[row['article_id']] = 1
        for row in fout:
            try:
                if okmap[row['article_id']] < 2:
                    continue
            except KeyError:
                continue
            if not row['cinema'].strip():
                continue
            if not row['title'].strip():
                continue
            tit=[]
            try:
                if len(set(article_cinemacheck[int(row['article_id'])])) > 3 or 'OK' in article_cinemacheck[int(row['article_id'])]:
                    out.append(row)
            except KeyError:
                
                pass
        
        #Now, tag everything that is probably a wrong city
        mm={}
        ct=0
        for row in out:
            ct+=1
            try:
                mm[row['article_id']]
            except KeyError:
                mm[row['article_id']]={}
            row['startposition']=int(row['startposition'])
            try:
                mm[row['article_id']][row['startposition']].append([row['cinema'],ct])
            except KeyError:
                mm[row['article_id']][row['startposition']]=[[row['cinema'],ct]]
        keepall = []
        remall=[]
        thiscins=list(nar.cinemas_this_city) + ['studio_2000']
        for aid, vv in mm.items():
            lst=[]
            for k,v in sorted(vv.items()):
                lst.append(v+[k])
            
            okgath=[]
            gath=[]
            keep=[]
            wantsect=[]
            previtm=None
            for j,b in enumerate(lst):
                ok=0
                if not b[0][0].strip():
                    continue
                previtm=b[0][0]
                nextitm=''
                try:
                    
                    cci=0
                    while not nextitm.strip():
                        cci+=1
                        nextitm = lst[j+cci][0][0]
                except IndexError:
                    pass
                try:
                    if previtm in thiscins:
                        ok=1
                except IndexError:
                    pass
                try:
                    if len(gath)>0 and nextitm in thiscins:
                        ok=1
                except IndexError:
                    pass
                if b[0][0] in nar.unique_to_city:
                    ok=1

                if ok == 0:
                    if len(gath)>len(okgath):
                        okgath=gath
                        keep=[]
                        for g in gath:
                            keep.append(g[-1])
                    gath=[]
                    continue
                else:
                    keep.append(b[-1])
                    gath.append(b)
            if len(gath)>len(okgath):
                okgath=gath

            if len(gath)>len(okgath):
                okgath=gath
                keep=[]
                for g in gath:
                    keep.append(g[-1])                
            woo=[]
            prev=None
            for zqg in sorted(okgath):
                if prev:
                    woo.append(zqg[-1]-prev)
                prev=zqg[-1]
            #No need for standard deviation - the extra stuff seems to amplify the mean enough
            mean = np.mean(woo)
            realokgath=[]
            realokgath=[]
            prev=None
            for zqg in sorted(okgath):
                if prev:
                    curval = zqg[-1]-prev
                    if curval < mean:
                        realokgath.append(zqg)
                prev=zqg[-1]
            prevvi=None
            rebar=[]
            foundcins=[]
            bins=[]
            for hh, xenu in enumerate(okgath):
                if xenu[0][0] in foundcins and not okgath[hh-1][0][0] == xenu[0][0]:
                    bins.append(rebar)
                    foundcins=[]
                    rebar=[]
                rebar.append(xenu)
                foundcins.append(xenu[0][0])
                prevvi=xenu
            bins.append(rebar)
            okgath = max(bins, key=len)
            keepall+=okgath
        newout=[]
        ct=0
        
        try:
            fff=['city_ok']+list(out[0].keys())
        except IndexError:
            pass
         
        for row in out:
            ct+=1
            if not [[row['cinema'], ct], row['startposition']] in keepall:
                row['city_ok'] = 0
            else:
                newout.append(row)
        
        with codecs.open('matches/%s/filtered/%s'% (city,xx), 'w', encoding='utf-8') as f:
            w=csv.DictWriter(f, delimiter=';', fieldnames=fff)
            w.writeheader()
            w.writerows(newout)
