import pickle
import unicodedata as ud

latin_letters= {}

def is_latin(uchr):
    try: return latin_letters[uchr]
    except KeyError:
         return latin_letters.setdefault(uchr, 'LATIN' in ud.name(uchr))

def only_roman_chars(unistr):
    return all(is_latin(uchr)
           for uchr in unistr
           if uchr.isalpha())
for i in range(1948,1996):
    dta = pickle.load(open('titcache/tits_%s.p' % i,'rb'))
    new={}
    tot=0
    bad=0
    for k,v in dta.items():
        tot+=1
        if not only_roman_chars(k):
            # print(k, v)
            bad+=1
        else:
            new[k]=v
    print(i,tot, bad)
    pickle.dump(new, open('titcache/tits_latin_%s.p' % i,'wb'))